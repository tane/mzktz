


#include <stdio.h>

#include "mzktz/base.h"


#include "mzktz/algorithm.h"

/// Check an error code and exit the application on non-success.
static void
check_error(mzktz_error_t e, const char *file, uint32_t line);
static void
check_error(mzktz_error_t e, const char *file, uint32_t line)
{
  if (MZKTZ_ESUCCESS != e) {
    mzktz_print_header("Application error", stderr);
    fprintf(
      stderr, "file: %s:%u\ncode: %d\nname: %s\n",
      file, line, e,
      mzktz_error_string(e)
    );
    exit(EXIT_FAILURE);
  }
}

#define CHECK_ERROR(e) { check_error(e, __FILE__, __LINE__); }


int
main(int argc, char **argv)
{
  (void)argc; (void)argv;

  mzktz_error_t e = 0;

  // Fetch ay platform,device pair..
  mzktz_pair_t pair;
  e = mzktz_pair_get(&pair);
  CHECK_ERROR(e);

  // Get me some context...
  mzktz_context_t c;
  e = mzktz_context_get(&c, pair.device_id);
  CHECK_ERROR(e);

  // Gimme stars and stripes...
  mzktz_queue_t q;
  e = mzktz_queue_get(&q, &c, pair.device_id);
  CHECK_ERROR(e);
  
  // Setup bucketsort.
  e = mzktz_algorithm_bucketsort_setup(&c);
  CHECK_ERROR(e);

  size_t len = 256;

  // Get a bucketsort instance.
  mzktz_algorithm_bucketsort_t bs;
  e = mzktz_algorithm_bucketsort_new(&bs, len);
  CHECK_ERROR(e);

  uint8_t data[len];
  for (size_t i = 0 ; i < len; i++)
    data[i] = (uint8_t)(i % 10);

  e = mzktz_algorithm_bucketsort_uint8(&bs, &q, data, (uint32_t)len);
  CHECK_ERROR(e);

  for (size_t i = 0; i < len; i++) {
    printf("%u\n", data[i]);
  }

  exit(EXIT_SUCCESS);
}
