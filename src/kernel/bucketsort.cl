kernel void bucketsort(
  global unsigned char *buffer,
  global uint *buckets,
  uint        size
)
{
  uint id = get_global_id(0);
  if (id < 256)
    buckets[id] = 0;
  if (id < size) {
    barrier(CLK_GLOBAL_MEM_FENCE);
    atomic_inc(buckets + buffer[id]);
  }
}


