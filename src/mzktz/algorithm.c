



#include "algorithm.h"

#include <assert.h>

/// Return the OpenCL error code if it isn't success.
#define MZKTZ_RETURN_ON_ERROR(error) if (error != CL_SUCCESS) return error

///
static mzktz_program_t mzktz_algorithm_bucketsort_program = 0;
///
static mzktz_kernel_t  mzktz_algorithm_bucketsort_kernel  = 0;
///
static mzktz_context_t mzktz_algorithm_bucketsort_context = 0;
///
static mzktz_memory_t  mzktz_algorithm_bucketsort_buckets = 0;


//
//
//
mzktz_error_t
mzktz_algorithm_bucketsort_setup(
  mzktz_context_t *context
)
{
  assert(0 == mzktz_algorithm_bucketsort_kernel);
  assert(0 == mzktz_algorithm_bucketsort_program);

  mzktz_error_t e = 0;

  // (1) Create program from source file.
  mzktz_program_t p;
  e = mzktz_program_get_from_file(&p, context, "kernel/bucketsort.cl");
  MZKTZ_RETURN_ON_ERROR(e);
  e = mzktz_program_build_and_log(&p, stderr);
  MZKTZ_RETURN_ON_ERROR(e);

  // (2) Create kernel.
  mzktz_kernel_t k;
  e = mzktz_kernel_get(&k, &p, "bucketsort");
  MZKTZ_RETURN_ON_ERROR(e);

  // (3) Create bucket buffer.
  cl_int error = 0;
  mzktz_memory_t b = clCreateBuffer(
    *context,
    CL_MEM_READ_WRITE,
    sizeof(cl_uint) * 256,
    0,
    &error
  );
  MZKTZ_RETURN_ON_ERROR(error);

  // (3) Copy resources.
  mzktz_algorithm_bucketsort_program = p;
  mzktz_algorithm_bucketsort_kernel  = k;
  mzktz_algorithm_bucketsort_context = *context;
  mzktz_algorithm_bucketsort_buckets = b;

  return MZKTZ_ESUCCESS;
}

//
//
//
mzktz_error_t
mzktz_algorithm_bucketsort_new(
  mzktz_algorithm_bucketsort_t *sorter,
  size_t                       size
)
{
  assert(0 != sorter);
  assert(0 < size);

  // (1) Create buffer.
  cl_int error = 0;
  mzktz_memory_t b = clCreateBuffer(
    mzktz_algorithm_bucketsort_context,
    CL_MEM_READ_WRITE,
    sizeof(uint8_t) * size,
    0,
    &error
  );
  MZKTZ_RETURN_ON_ERROR(error);

  // (2) Assign resource.
  sorter->size   = size;
  sorter->buffer = b;

  return MZKTZ_ESUCCESS;
}
void
check_error(mzktz_error_t e, const char *file, uint32_t line);
void
check_error(mzktz_error_t e, const char *file, uint32_t line)
{
  if (MZKTZ_ESUCCESS != e) {
    mzktz_print_header("Application error", stderr);
    fprintf(
      stderr, "file: %s:%u\ncode: %d\nname: %s\n",
      file, line, e,
      mzktz_error_string(e)
    );
    exit(EXIT_FAILURE);
  }
}

#define CHECK_ERROR(e) { check_error(e, __FILE__, __LINE__); }

//
//
//
mzktz_error_t
mzktz_algorithm_bucketsort_uint8(
  mzktz_algorithm_bucketsort_t *sorter,
  mzktz_queue_t                *queue,
  uint8_t                      *values,
  uint32_t                     size
)
{
  assert(0 != sorter);
  assert(0 != values);
  assert(0 < size);
  assert(size <= sorter->size);

  cl_int error = 0;

  // (1) Enqueue copy.
  error = clEnqueueWriteBuffer(
    *queue,
    sorter->buffer,
    CL_FALSE,
    0,
    sizeof(uint8_t) * size,
    values,
    0, 0, 0);
  CHECK_ERROR(error);
  MZKTZ_RETURN_ON_ERROR(error);

  error = 0;
  error  = clSetKernelArg(
    mzktz_algorithm_bucketsort_kernel,
    0, sizeof(cl_mem), &sorter->buffer
  );
  error |= clSetKernelArg(
    mzktz_algorithm_bucketsort_kernel,
    1, sizeof(cl_mem), &mzktz_algorithm_bucketsort_buckets
  );
  error |= clSetKernelArg(
    mzktz_algorithm_bucketsort_kernel,
    2, sizeof(uint32_t), &size
  );
  CHECK_ERROR(error);
  MZKTZ_RETURN_ON_ERROR(error);
  
  size_t global = (size_t)size;
  error = clEnqueueNDRangeKernel(
    *queue,
    mzktz_algorithm_bucketsort_kernel,
    1, NULL, &global, NULL, 0, NULL, NULL
  );
  CHECK_ERROR(error);
  MZKTZ_RETURN_ON_ERROR(error);

  clFinish(*queue);

  cl_uint buckets[256];
  error = clEnqueueReadBuffer(
    *queue,
    mzktz_algorithm_bucketsort_buckets,
    CL_TRUE,
    0, sizeof(cl_uint) * 256,
    buckets, 0, NULL, NULL);
  CHECK_ERROR(error);
  MZKTZ_RETURN_ON_ERROR(error);
  clFinish(*queue);
  
  size_t pos = 0;
  for (size_t i = 0; i < 256; i++) {
    for (size_t n = buckets[i]; n > 0; n--)
      values[pos++] = (uint8_t)i;
  }

  return MZKTZ_ESUCCESS;
}
