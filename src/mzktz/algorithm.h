
#ifndef MZKTZ_ALGORITHM_H
#define MZKTZ_ALGORITHM_H

#include "base.h"

#include <inttypes.h>


typedef uint32_t mzktz_algorithm_bucketsort_counter_t;





///
///
///
typedef struct mzktz_algorithm_bucketsort_t {
  /// Number of elements.
  size_t         size;
  /// Result buckets.
  mzktz_memory_t buffer;
} mzktz_algorithm_bucketsort_t;


///
///
///
///
mzktz_error_t
mzktz_algorithm_bucketsort_setup(
  mzktz_context_t *context
);



///
/// Creates a new bucketsort structure for an unsigned integer type of
/// the given size.
/// @param sorter    Sort structure for buffer objects.
/// @return  Error code implying state of operation.
mzktz_error_t
mzktz_algorithm_bucketsort_new(
  mzktz_algorithm_bucketsort_t *sorter,
  size_t                       size
);


///
///
///
///
mzktz_error_t
mzktz_algorithm_bucketsort_uint8(
  mzktz_algorithm_bucketsort_t *sorter,
  mzktz_queue_t                *queue,
  uint8_t                      *values,
  uint32_t                     size
);



#endif // MZKTZ_ALGORITHM_H
