
#include "base.h"

#include <assert.h>

// stat()
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

/// Return the OpenCL error code if it isn't success.
#define MZKTZ_RETURN_ON_ERROR(error) if (error != CL_SUCCESS) return error

const mzktz_error_t MZKTZ_ESUCCESS      = 0;
const mzktz_error_t MZKTZ_ENOPLATFORMS  = 1;
const mzktz_error_t MZKTZ_ENODEVICES    = 2;
const mzktz_error_t MZKTZ_ECOULDNOTSTAT = 3;
const mzktz_error_t MZKTZ_ENOTAFILE     = 4;
const mzktz_error_t MZKTZ_EEMPTYFILE    = 5;
const mzktz_error_t MZKTZ_ECOULDNOTOPEN = 6;
const mzktz_error_t MZKTZ_ECOULDNOTREAD = 7;


const char *MZKTZ_ERROR_STRINGS[7] = {
  "Success",
  "No platforms available",
  "No devices available",
  "Could not stat() filepath",
  "Filepath does not point to a regular file",
  "Could not open filepath",
  "Error while reading file"
};

const char *MZKTZ_ERROR_OPENCL_STRINGS[59] = {
  "CL_SUCCESS",
  "CL_DEVICE_NOT_FOUND",
  "CL_DEVICE_NOT_AVAILABLE",
  "CL_COMPILER_NOT_AVAILABLE",
  "CL_MEM_OBJECT_ALLOCATION_FAILURE",
  "CL_OUT_OF_RESOURCES",
  "CL_OUT_OF_HOST_MEMORY",
  "CL_PROFILING_INFO_NOT_AVAILABLE",
  "CL_MEM_COPY_OVERLAP",
  "CL_IMAGE_FORMAT_MISMATCH",
  "CL_IMAGE_FORMAT_NOT_SUPPORTED",
  "CL_BUILD_PROGRAM_FAILURE",
  "CL_MAP_FAILURE",
  "CL_MISALIGNED_SUB_BUFFER_OFFSET",
  "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST"  // -14
  "", "", "", "", "", "", // -20
  "", "", "", "", "",     // -25
  "", "", "", "",         // -29
  "CL_INVALID_VALUE",
  "CL_INVALID_DEVICE_TYPE",
  "CL_INVALID_PLATFORM",
  "CL_INVALID_DEVICE",
  "CL_INVALID_CONTEXT",
  "CL_INVALID_QUEUE_PROPERTIES",
  "CL_INVALID_COMMAND_QUEUE",
  "CL_INVALID_HOST_PTR",
  "CL_INVALID_MEM_OBJECT",
  "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR",
  "CL_INVALID_IMAGE_SIZE",
  "CL_INVALID_SAMPLER",
  "CL_INVALID_BINARY",
  "CL_INVALID_BUILD_OPTIONS",
  "CL_INVALID_PROGRAM",
  "CL_INVALID_PROGRAM_EXECUTABLE",
  "CL_INVALID_KERNEL_NAME",
  "CL_INVALID_KERNEL_DEFINITION",
  "CL_INVALID_KERNEL",
  "CL_INVALID_ARG_INDEX",
  "CL_INVALID_ARG_VALUE",
  "CL_INVALID_ARG_SIZE",
  "CL_INVALID_KERNEL_ARGS",
  "CL_INVALID_WORK_DIMENSION",
  "CL_INVALID_WORK_GROUP_SIZE",
  "CL_INVALID_WORK_ITEM_SIZE",
  "CL_INVALID_GLOBAL_OFFSET",
  "CL_INVALID_EVENT_WAIT_LIST",
  "CL_INVALID_EVENT"
};


const char *MZKTZ_H1 =
"==========================================================================\n";
const char *MZKTZ_H2 =
"--------------------------------------------------------------------------\n";
const char *MZKTZ_H3 =
"··········································································\n";
//"..........................................................................\n";
//"--    --    --    --    --    --    --    --    --    --    --    --    --\n";

void
mzktz_print_header(const char *message, FILE *stream)
{
  fprintf(stream, "%s%s\n%s", MZKTZ_H1, message, MZKTZ_H2);
}

mzktz_error_t
mzktz_context_get(mzktz_context_t *context, mzktz_device_t dev)
{
  assert(0 != context);

  cl_int error = 0;
  cl_context ctx = clCreateContext(0, 1, &dev, 0, 0, &error);
  MZKTZ_RETURN_ON_ERROR(error);
  *context = ctx;
  return MZKTZ_ESUCCESS;
}

//
//
//
const char *
mzktz_error_string(mzktz_error_t error)
{
  if (error < 0) {
    // OpenCL error code.
    return MZKTZ_ERROR_OPENCL_STRINGS[-1*error];
  } else {
    // Mzktz opencl error code.
    return MZKTZ_ERROR_STRINGS[error];
  }
}

//
//
//
mzktz_error_t
mzktz_kernel_get(
  mzktz_kernel_t  *kernel,
  mzktz_program_t *program,
  const char      *name
)
{
  assert(0 != kernel);
  assert(0 != program);
  assert(0 != name);

  cl_int error = 0;
  // (1) Create kernel.
  cl_kernel k = clCreateKernel(*program, name, &error);
  MZKTZ_RETURN_ON_ERROR(error);

  // (2) Store kernel.
  *kernel = k;
  

  return MZKTZ_ESUCCESS;
}

//
//
//
mzktz_error_t
mzktz_pair_get(mzktz_pair_t *pair)
{
  assert(0 != pair);

  // (1) Fetch first platform
  // (2) Fetch its first device
  // (3) Store both and return success.
  
  // (1.1) Fetch number of platforms.
  cl_int status = 0;
  cl_uint np = 0;
  status = clGetPlatformIDs(0, 0, &np);
  MZKTZ_RETURN_ON_ERROR(status);
  if (np == 0) return MZKTZ_ENOPLATFORMS;

  // (1.2) Fetch platform id.
  cl_platform_id pfid = 0;
  status = clGetPlatformIDs(1, &pfid, 0);
  MZKTZ_RETURN_ON_ERROR(status);

  assert(0 != pfid);

  // (2.1) Fetch device for platform.
  status = clGetDeviceIDs(pfid, CL_DEVICE_TYPE_GPU, 0, 0, &np);
  MZKTZ_RETURN_ON_ERROR(status);
  if (0 == np) return MZKTZ_ENODEVICES;
  cl_device_id dvid = 0;
  status = clGetDeviceIDs(pfid, CL_DEVICE_TYPE_GPU, 1, &dvid, 0);
  MZKTZ_RETURN_ON_ERROR(status);

  assert(0 != dvid);

  // (3) Store IDs to structure.
  pair->platform_id = pfid;
  pair->device_id   = dvid;

  return MZKTZ_ESUCCESS;
}

//
//
//
mzktz_error_t
mzktz_program_build(mzktz_program_t *program)
{
  assert(0 != program);

  cl_int error = clBuildProgram(*program, 0, 0, 0, 0, 0);
  MZKTZ_RETURN_ON_ERROR(error);
  
  return MZKTZ_ESUCCESS;
}

mzktz_error_t
mzktz_program_build_and_log(mzktz_program_t *program, FILE *stream)
{
  assert(0 != program);
  assert(0 != stream);
  
  // (1) Get program's device.
  cl_device_id dvid = 0;
  mzktz_error_t e = clGetProgramInfo(
    *program,
    CL_PROGRAM_DEVICES,
    sizeof(cl_device_id), &dvid, 0
  );
  assert(MZKTZ_ESUCCESS == e);
  MZKTZ_RETURN_ON_ERROR(e);


  // (2) Build program.
  e = mzktz_program_build(program);

  mzktz_print_header("Program build log", stream);
  
  // (3.1) Get build status.
  {
    cl_build_status bs = 0;
    clGetProgramBuildInfo(
      *program,
      dvid,
      CL_PROGRAM_BUILD_STATUS,
      sizeof(cl_build_status), &bs, 0
    );
    const char *status_strings[] = {
      "none", "error", "success", "in progress", "unknown"
    };
    size_t status_id = 4;
    switch (bs) {
      case CL_BUILD_NONE:         status_id = 0; break;
      case CL_BUILD_ERROR:        status_id = 1; break;
      case CL_BUILD_SUCCESS:      status_id = 2; break;
      case CL_BUILD_IN_PROGRESS:  status_id = 3; break;
    }
    fprintf(stream, "status: %s\n%s", status_strings[status_id], MZKTZ_H3);
  }

  // (3.) Get log size.
  size_t log_length = 0;
  clGetProgramBuildInfo(
    *program,
    dvid,
    CL_PROGRAM_BUILD_LOG,
    0, 0, &log_length
  );


  if (log_length > 0) {
    char buf[log_length+1];
    clGetProgramBuildInfo(
      *program,
      dvid,
      CL_PROGRAM_BUILD_LOG,
      log_length, buf, 0
    );
    buf[log_length-1] = 0;
    fprintf(stream, "%s", buf);
  }
  return e;
}

//
//
//
mzktz_error_t
mzktz_program_get_from_file(
  mzktz_program_t *program,
  mzktz_context_t *context,
  const char      *source_path
)
{
  assert(0 != program);
  assert(0 != source_path);
  
  // (1) Stat source file.
  // (2) Load source file.
  // (3) Create program.
  // (4) Store program.

  // (1.1) Stat source file.
  struct stat file_info;
  if (stat(source_path, &file_info) == -1)
    return MZKTZ_ECOULDNOTSTAT;
  
  // (1.2) Check it for being a regular file.
  if (!S_ISREG(file_info.st_mode))
    return MZKTZ_ENOTAFILE;

  // (1.3) Fetch file size.
  size_t fsize = (size_t)file_info.st_size;
  if (0 >= fsize)
    return MZKTZ_EEMPTYFILE;

  // (2) Load file to buffer.
  char buf[fsize];
  {
    FILE *fp = fopen(source_path, "r");
    if (0 == fp)
      return MZKTZ_ECOULDNOTOPEN;
    size_t bytes_read = fread(buf, 1, (size_t)fsize, fp);
    buf[bytes_read-1] = '\0';
    fclose(fp);
    if (bytes_read != (size_t)fsize) {
      return MZKTZ_ECOULDNOTREAD;
    }
  }

  // (3) Create program.
  cl_int error = 0;
  const char *buf_ptr = buf;
  cl_program p = clCreateProgramWithSource(*context, 1, (const char **)&buf_ptr, 0, &error);
  MZKTZ_RETURN_ON_ERROR(error);
  
  // (4) Store program.
  *program = p;

  return MZKTZ_ESUCCESS;
}

mzktz_error_t
mzktz_queue_get(
  mzktz_queue_t   *queue,
  mzktz_context_t *context,
  mzktz_device_t  device
)
{
  assert(0 != queue);

  cl_int error = 0;
  cl_command_queue q = clCreateCommandQueue(*context, device, 0, &error);
  MZKTZ_RETURN_ON_ERROR(error);
  *queue = q;
  return MZKTZ_ESUCCESS;
}

//
//
//
void
mzktz_infobox(void)
{
    cl_int status;
    cl_uint num;

    status = clGetPlatformIDs(0, NULL, &num);
    printf("status %u num=%u\n", status, num);
    cl_platform_id platformIDs[num];
    status = clGetPlatformIDs(num, platformIDs, NULL);

    for (unsigned int i = 0; i < num; i++) {
        cl_uint num_devices;
        status = clGetDeviceIDs(platformIDs[i],
                CL_DEVICE_TYPE_ALL,
                0,
                NULL,
                &num_devices);
        if (status == CL_SUCCESS) {
            // found devices, fetch and print them
            cl_device_id device_ids[num_devices];
            status = clGetDeviceIDs(platformIDs[i],
                    CL_DEVICE_TYPE_ALL,
                    num_devices,
                    device_ids,
                    NULL);
            if (status == CL_SUCCESS) {
                for (unsigned int d = 0; d < num_devices; d++) {
                    cl_device_type dev_type;
                    status = clGetDeviceInfo(device_ids[d],
                            CL_DEVICE_TYPE,
                            sizeof(cl_device_type),
                            &dev_type,
                            NULL);
                    if (status == CL_SUCCESS) {
                        if (CL_DEVICE_TYPE_CPU & dev_type) {
                            printf("got a CPU\n");
                        } else if (CL_DEVICE_TYPE_GPU & dev_type) {
                            char buf[512];
                            clGetDeviceInfo(device_ids[d],
                                    CL_DEVICE_NAME,
                                    sizeof(buf),
                                    buf, NULL);
                            printf("got a GPU: %s\n",buf);
                        }
                    }
                }

            }
        }
    } 


}



