




#ifndef MZKTZ_BASE_H
#define MZKTZ_BASE_H

#include <CL/cl.h>

#include <inttypes.h>
#include <stdio.h>

/// Pair type for a platform and a corresponding device.
typedef struct mzktz_pair_t {
  /// Pair's platform id.
  cl_platform_id  platform_id;
  /// Pair's device id.
  cl_device_id    device_id;
} mzktz_pair_t;


///
typedef cl_device_id mzktz_device_t;
///
typedef cl_context mzktz_context_t;
///
typedef cl_command_queue mzktz_queue_t;
///
typedef cl_program mzktz_program_t;
///
typedef cl_kernel mzktz_kernel_t;

typedef cl_mem mzktz_memory_t;


/// Error code type.
typedef int32_t mzktz_error_t;

/// Error codes.
extern const mzktz_error_t MZKTZ_ESUCCESS;
extern const mzktz_error_t MZKTZ_ENOPLATFORMS;
extern const mzktz_error_t MZKTZ_ENODEVICES;
extern const mzktz_error_t MZKTZ_ECOULDNOTSTAT;
extern const mzktz_error_t MZKTZ_ENOTAFILE;
extern const mzktz_error_t MZKTZ_EEMPTYFILE;
extern const mzktz_error_t MZKTZ_ECOULDNOTOPEN;
extern const mzktz_error_t MZKTZ_ECOULDNOTREAD;


extern const char *MZKTZ_H1;
extern const char *MZKTZ_H2;
extern const char *MZKTZ_H3;

void
mzktz_print_header(const char *message, FILE *stream);


///
/// Retrieves a string representation of a given error.
/// @param error  Error code.
/// @return  Pointer to constant memory location holding 0-terminated
///          string representation of the given error code.
const char *
mzktz_error_string(mzktz_error_t error);

///
/// Fetches the first platform and its first device and stores their IDs in
/// the given structure.
/// @param pair  Pair structure to store IDs in.
/// @return  Error code implying state of operation. If the code is any other
///          than MZKTZ_ESUCCESS, the resulting pair structure should not
///          be used any further.
mzktz_error_t
mzktz_pair_get(mzktz_pair_t *pair);

mzktz_error_t
mzktz_context_get(mzktz_context_t *context, mzktz_device_t dev);



///
/// Creates a kernel of given name from the given program.
/// @param kernel   Kernel type to store in.
/// @param program  Program the kernel is stored in.
/// @param name     Kernel's name,
/// @return  Error code implying state of operation.
mzktz_error_t
mzktz_kernel_get(
  mzktz_kernel_t  *kernel,
  mzktz_program_t *program,
  const char      *name
);


///
/// Builds the given program.
/// @param program  Program structure.
/// @return Error code implying state of operation.
mzktz_error_t
mzktz_program_build(mzktz_program_t *program);

mzktz_error_t
mzktz_program_build_and_log(mzktz_program_t *program, FILE *stream);

///
/// Loads the source from the given path and creates a program.
/// @param program      Program structure.
/// @param context      Context to create program in.
/// @param source_path  Path to program's source file.
/// @return  Error code implying state.
mzktz_error_t
mzktz_program_get_from_file(
  mzktz_program_t *program,
  mzktz_context_t *context,
  const char      *source_path
);

mzktz_error_t
mzktz_queue_get(
  mzktz_queue_t   *queue,
  mzktz_context_t *context,
  mzktz_device_t  device
);

//

void
mzktz_infobox(void);


#endif // MZKTZ_BASE_H
